package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestTraceLines(t *testing.T) {
	lines := []string{
		// HTML:
		`<img src="images/monkey_grid_qr.webp" alt="QR code for the link URL" style="width: 13ex;" />`,
		`<section data-background-video="videos/blender_logo.webm" data-state-nah="next-after-video"></section>`,
		`<video data-src="images/blender-splash-screens.webm" data-autoplay="false"></video>`,
		`<h1 class="r-fit-text"><img src="images/blender-logo.svg" alt="Blender Logo" id="blenderlogo"> <strong>&amp;</strong> <img src="images/python-logo.svg" alt="Python Logo" id="pythonlogo"></h1>`,
		// MarkDown:
		`![](images/people-remote.webp)`,
		`![Alt text about images/videos and other stuff](images/money-fund-individual.webp)`,
		`<!-- .slide: data-background-image="images/2020-05-05-planet-tulpen.webp" data-background-size="contain" data-state="nologo" -->`,
	}
	expectedFilenames := []string{
		"images/monkey_grid_qr.webp",
		"videos/blender_logo.webm",
		"images/blender-splash-screens.webm",
		"images/blender-logo.svg",
		"images/python-logo.svg",
		"images/people-remote.webp",
		"images/money-fund-individual.webp",
		"images/2020-05-05-planet-tulpen.webp",
	}

	tracer := Tracer{
		RootPath: "root",
		filename: "index.html",
		subdirs:  []string{"images/", "videos/"},
	}

	usedAssets, err := tracer.traceLines(lines)
	require.NoError(t, err)
	assert.Equal(t, expectedFilenames, usedAssets)
}
