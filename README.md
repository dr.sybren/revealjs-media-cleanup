# RevealJS Media Cleanup

**Use at your own risk!**

## Installation

```sh
go install gitlab.com/dr.sybren/revealjs-media-cleanup@latest
```

## Running

```sh
# Get a list of unused assets:
revealjs-media-cleanup /path/to/revealjs/index.html

# Delete the assets one by one, 2 per second,
# so you can abort with minimal loss in case of panic:
revealjs-media-cleanup -delete /path/to/revealjs/index.html

# Delete things as quickly as possible:
revealjs-media-cleanup -delete -fast /path/to/revealjs/index.html
```

This assumes that all your images and videos and other assets you want to keep
are in a subdirectory called `images/` or `videos/`. You can mix those, so
videos in `images/` is fine, and vice versa. It's just about the name of the
sub-directories, not the kind of file.

## Warning

**Use at your own risk!**
