package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/rs/zerolog/log"
)

type Tracer struct {
	RootPath string
	filename string

	subdirs []string
}

func revealJSTracer(htmlFilePath string, subdirs []string) Tracer {
	rootPath, filename := filepath.Split(htmlFilePath)
	return Tracer{
		RootPath: rootPath,
		filename: filename,
		subdirs:  subdirs,
	}
}

func (t Tracer) FindUnusedAssets() ([]string, error) {
	usedAssets, err := t.traceUsedAssets()
	if err != nil {
		return nil, fmt.Errorf("couldn't find used assets: %w", err)
	}

	allAssets, err := t.findAllAssets()
	if err != nil {
		return nil, fmt.Errorf("couldn't find all assets: %w", err)
	}

	unusedAssets := []string{}
	for _, asset := range allAssets {
		if isUsed(asset, usedAssets) {
			continue
		}
		log.Debug().Str("path", asset).Msg("unused asset")
		unusedAssets = append(unusedAssets, asset)
	}

	return unusedAssets, nil
}

func (t Tracer) traceUsedAssets() ([]string, error) {
	log.Info().
		Str("root", t.RootPath).
		Str("html", t.filename).
		Msg("tracing HTML file")

	lines, err := t.readLines()
	if err != nil {
		return nil, err
	}

	return t.traceLines(lines)
}

func (t Tracer) findAllAssets() ([]string, error) {
	filenames := []string{}

	for _, subdir := range t.subdirs {
		fullSubdirPath := filepath.Join(t.RootPath, subdir)
		subdirFiles, err := findAssetsInDir(fullSubdirPath)
		if err != nil {
			return nil, err
		}

		for _, filename := range subdirFiles {
			relPath, err := filepath.Rel(t.RootPath, filename)
			if err != nil {
				panic(fmt.Sprintf("found asset %q is not in root %q: %v", filename, t.RootPath, err))
			}

			filenames = append(filenames, relPath)
		}

	}

	return filenames, nil
}

func (t Tracer) traceLines(lines []string) ([]string, error) {
	// Construct the regexp, assuming that 'subdirs' only has simple strings like "images/", "videos/", etc.
	expression := fmt.Sprintf(`(?:["(])((?:%s)[^")]*)`, strings.Join(t.subdirs, "|"))
	re, err := regexp.Compile(expression)
	if err != nil {
		return nil, fmt.Errorf("compiling regular expression %q: %w", expression, err)
	}

	// Parse each line and produce media filenames.
	foundAssets := []string{}
	for lineindex, line := range lines {
		line = strings.TrimSpace(line)
		if line == "" {
			continue
		}

		log.Debug().Int("linenum", lineindex+1).Str("line", line).Msg("parsing line")

		matches := re.FindAllStringSubmatch(line, -1)
		if len(matches) == 0 {
			continue
		}

		for _, match := range matches {
			filename := match[1]
			log.Info().
				Int("linenum", lineindex+1).
				Str("filename", filename).
				Msg("asset reference")
			foundAssets = append(foundAssets, filename)
		}
	}

	return foundAssets, nil
}

func (t Tracer) readLines() ([]string, error) {
	// Read the file into memory.
	htmlFilePath := filepath.Join(t.RootPath, t.filename)
	contents, err := os.ReadFile(htmlFilePath)
	if err != nil {
		return nil, fmt.Errorf("reading %s: %w", htmlFilePath, err)
	}

	// Parse into lines.
	lines := strings.Split(string(contents), "\n")
	return lines, nil
}

func findAssetsInDir(fullSubdirPath string) ([]string, error) {
	glob := filepath.Join(fullSubdirPath, "*")
	filenamesInSub, err := filepath.Glob(glob)
	if err != nil {
		return nil, fmt.Errorf("finding files in %q: %w", glob, err)
	}

	assetFilenames := []string{}

	for _, filename := range filenamesInSub {
		stat, err := os.Stat(filename)
		switch {
		case err != nil:
			log.Warn().Str("filepath", filename).Msg("could not read file, skipping")
			continue
		case stat.IsDir():
			log.Warn().Str("path", filename).Msg("not supporting subdirectories yet")
		default:
			assetFilenames = append(assetFilenames, filename)
		}
	}

	return assetFilenames, nil
}

func isUsed(someAsset string, usedAssets []string) bool {
	for _, path := range usedAssets {
		if path == someAsset {
			return true
		}
	}
	return false
}
