package main

// SPDX-License-Identifier: GPL-3.0-or-later

import (
	"errors"
	"flag"
	"os"
	"path/filepath"
	"time"

	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

var subdirs = []string{
	"images/",
	"videos/",
}

var doDelete bool
var doDeleteFast bool

func main() {
	output := zerolog.ConsoleWriter{Out: colorable.NewColorableStdout(), TimeFormat: time.RFC3339}
	log.Logger = log.Output(output)
	filename := parseCliArgs()

	tracer := revealJSTracer(filename, subdirs)

	unusedAssets, err := tracer.FindUnusedAssets()
	if err != nil {
		log.Fatal().Err(err).Msg("couldn't find unused assets")
	}

	for _, asset := range unusedAssets {
		log.Info().Str("path", asset).Msg("unused asset")
	}

	if !doDelete {
		log.Info().Int("unusedAssets", len(unusedAssets)).Msg("done, nothing actually got deleted")
		return
	}

	// Actually delete the unused file.
	for _, asset := range unusedAssets {
		fullPath := filepath.Join(tracer.RootPath, asset)
		log.Info().Str("path", fullPath).Msg("deleting")

		if !doDeleteFast {
			time.Sleep(500 * time.Millisecond)
		}
		if err := os.Remove(fullPath); err != nil {
			log.Info().Str("path", fullPath).Err(err).Msg("error deleting file, continuing with the next")
		}
	}

	log.Info().Msg("done!")
}

func parseCliArgs() string {
	var quiet, debug, trace bool

	flag.BoolVar(&quiet, "quiet", false, "Only log warning-level and worse.")
	flag.BoolVar(&debug, "debug", false, "Enable debug-level logging.")
	flag.BoolVar(&trace, "trace", false, "Enable trace-level logging.")
	flag.BoolVar(&doDelete, "delete", false, "Actually delete unused assets.")
	flag.BoolVar(&doDeleteFast, "fast", false, "Do not delay between deleting files, go as quickly as possible.")

	flag.Parse()

	var logLevel zerolog.Level
	switch {
	case trace:
		logLevel = zerolog.TraceLevel
	case debug:
		logLevel = zerolog.DebugLevel
	case quiet:
		logLevel = zerolog.WarnLevel
	default:
		logLevel = zerolog.InfoLevel
	}
	zerolog.SetGlobalLevel(logLevel)

	// Get the filename.
	filename := flag.Arg(0)
	if filename == "" {
		flag.Usage()
		log.Fatal().Msg("provide a HTML filename")
	}

	// Check the given filename.
	filename, err := filepath.Abs(filename)
	if err != nil {
		log.Fatal().Err(err).Str("filename", filename).Msg("could not make this an absolute path")
	}

	stat, err := os.Stat(filename)
	switch {
	case errors.Is(err, os.ErrNotExist):
		log.Fatal().Str("filename", filename).Msg("given file does not exist")
	case err != nil:
		log.Fatal().Str("filename", filename).Err(err).Msg("there is an issue with this file")
	case stat.IsDir():
		log.Fatal().Str("argument", filename).Msg("give the path to the HTML file, this is a directory")
	default:
		log.Info().Str("filename", filename).Msg("operating on given file")
	}

	return filename
}
